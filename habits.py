import os
import re
import logging
from datetime import datetime, timedelta, date
from todoist.api import TodoistAPI

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

TODOIST_DATE_FORMAT = "%Y-%m-%d"

def get_token():
    token = os.getenv('TODOIST_APIKEY')
    return token


def is_habit(text):
    return re.search(r'\[day\s(\d+)\]', text)

def today_date():
    return datetime.utcnow().strftime(TODOIST_DATE_FORMAT)

def is_today(text):
    text = to_date(text)
    today = datetime.utcnow().strftime(TODOIST_DATE_FORMAT)
    return text == today


def to_date(text):
    try:  # We try to parse a date
        text = date.fromisoformat(text).strftime(TODOIST_DATE_FORMAT)
    except ValueError:  # If it is not a date we assume it is a datetime
        if not text[-1] == "Z":
            raise AssertionError("Expected UTC datetime")
        text = datetime.fromisoformat(text[:-1]).strftime(TODOIST_DATE_FORMAT)
    return text


def is_due_yesterday(text):
    text = to_date(text)
    yesterday = (datetime.utcnow() - timedelta(1)).strftime(TODOIST_DATE_FORMAT)
    return text == yesterday


def update_streak(task, streak):
    days = '[day {}]'.format(streak)
    text = re.sub(r'\[day\s(\d+)\]', days, task['content'])
    task.update(content=text)


def main():
    out = ""

    API_TOKEN = get_token()
    today = datetime.utcnow().replace(tzinfo=None)

    if not API_TOKEN:
        logging.warning('Please set the API token in environment variable.')
        exit()

    api = TodoistAPI(API_TOKEN)
    api.sync()
    tasks = api.state['items']

    out += "Number of tasks : " + str(len(tasks)) + "\n"

    for task in tasks:
        if task['due'] and is_habit(task['content']) and not task['in_history']:
            out += "Task " + task['content']+ ": "

            if is_today(task['due']['date']):
                habit = is_habit(task['content'])
                streak = int(habit.group(1)) + 1
                update_streak(task, streak)

                out += "completed --> +1"
            elif is_due_yesterday(task['due']['date']):
                update_streak(task, 0)
                task.update(due={'string': 'ev day starting {}'.format(today_date())})

                out += "not completed --> 0"
            else:
                out += "no processed"

            out += "\n"

    api.commit()
    return out

if __name__ == '__main__':
    out = main()
    print(out)
