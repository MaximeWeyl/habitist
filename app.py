import os

from flask import Flask, request
import habits

app = Flask(__name__)


@app.route('/')
def main():
    secret_url = request.args.get("secret", "NO_URL_SECRET")
    secret_config = os.environ['APP_SECRET']

    if secret_config == secret_url:
        return habits.main()

    else:
        return "Unauthorized"


if __name__ == '__main__':
    app.run()
